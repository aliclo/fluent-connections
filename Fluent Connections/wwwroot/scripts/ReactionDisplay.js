﻿const MAX_DOTS = 3;
const DOTS_UPDATE_MILLISECONDS = 500;

class ChoosableGroup {
    constructor(topNode, components) {
        this.topNode = topNode;
        this.components = components;
        this.chosenCallbacks = [];
        this.unchosenCallbacks = [];
        this.firstSelectionCallbacks = [];
        this.lastDeselectionCallbacks = [];
        this.count = 0;

        var group = this;

        this.topNode.on("mouseenter", this.components, function () {
            if (!$(this).hasClass("option-chosen")) {
                $(this).removeClass("option-unselected")
                    .addClass("option-selected");
            }

            if (!$(this).parent().hasClass("option-chosen")
                && $(this).parent().hasClass("option-selected")) {

                $(this).parent().removeClass("option-selected")
                    .addClass("option-unselected");
            }
        });

        this.topNode.on("mouseleave", this.components, function () {
            if (!$(this).hasClass("option-chosen")) {
                $(this).removeClass("option-selected")
                    .addClass("option-unselected");
            }

            if (!$(this).parent().hasClass("option-chosen")
                && $(this).parent().hasClass("option-unselected")) {

                $(this).parent().removeClass("option-unselected")
                    .addClass("option-selected");
            }
        });

        this.topNode.on("click", this.components, function (e) {
            if ($(e.target).parents(".option-overlay").length) {
                return true;
            }

            if ($(this).hasClass("option-chosen")) {
                $(this).removeClass("option-chosen");
                group.elementDeselected(group, this);
            } else {
                $(this).addClass("option-chosen");
                group.elementSelected(group, this);
            }

            return true;
        });
    }

    elementDeselected(group, deselectedElem) {
        group.count--;

        if (group.count == 0) {
            var numOfCallbacks = group.lastDeselectionCallbacks.length;
            for (var i = 0; i < numOfCallbacks; i++) {
                group.lastDeselectionCallbacks[i](this, deselectedElem);
            }
        }

        for (var i = 0; i < group.unchosenCallbacks.length; i++) {
            group.unchosenCallbacks[i](this, deselectedElem);
        }
    }

    elementSelected(group, selectedElem) {
        group.count++;

        if (group.count == 1) {
            var numOfCallbacks = group.firstSelectionCallbacks.length;
            for (var i = 0; i < numOfCallbacks; i++) {
                group.firstSelectionCallbacks[i](this, selectedElem);
            }
        }

        for (var i = 0; i < group.chosenCallbacks.length; i++) {
            group.chosenCallbacks[i](this, selectedElem);
        }
    }

    addChosenCallback(callback) {
        this.chosenCallbacks.push(callback);
    }

    addUnchosenCallback(callback) {
        this.unchosenCallbacks.push(callback);
    }

    addFirstSelectionCallback(callback) {
        this.firstSelectionCallbacks.push(callback);
    }

    addLastDeselectionCallback(callback) {
        this.lastDeselectionCallbacks.push(callback);
    }

    unchooseAllComponents() {
        this.topNode.find(this.components).removeClass("option-chosen")
            .removeClass("option-selected").addClass("option-unselected");
    }

    getChosenElements() {
        return this.topNode.find(this.components + ".option-chosen");
    }

    deleteSelectedElements() {
        this.count = 0;
        this.topNode.find(this.components + ".option-chosen").remove();

        var numOfCallbacks = this.lastDeselectionCallbacks.length;
        for (var i = 0; i < numOfCallbacks; i++) {
            this.lastDeselectionCallbacks[i](null);
        }
    }
}

class PopupBox {

    constructor(name, title) {
        this.name = name;
        this.title = title;
        this.contents = "";
        this.onHideCallbacks = [];
    }

    setContents(contents) {
        this.contents = contents;
    }

    hide() {
        $("#" + this.name + "-back").removeClass("show-popup")
            .addClass("hide-popup");
    }

    show() {
        $("#" + this.name + "-back").removeClass("hide-popup")
            .addClass("show-popup");
    }

    getHTML() {
        return (
            "<div id='" + this.name + "-back' " +
            "    class='popup-back hide-popup'>" +
            "  <div id='" + this.name + "-content' class='popup-content'>" +
            "    <h2 class='common-heading'>" + this.title + "</h2>" +
            "    <div class='center-card-contents'>" + this.contents +
            "    </div>" +
            "  </div>" +
            "</div>");
    }

    write() {
        document.write(this.getHTML());
        var popupBox = this;

        $("#" + this.name + "-back").click(function (e) {
            if (e.target == this) {
                $(this).removeClass("show-popup").addClass("hide-popup");

                for (var i = 0; i < popupBox.onHideCallbacks.length; i++) {
                    popupBox.onHideCallbacks[i](popupBox.name);
                }
            }
        });
    }

    addOnHideCallback(callback) {
        this.onHideCallbacks.push(callback);
    }

}

class ChoicePopupBox extends PopupBox {

    constructor(name, title) {
        super(name, title);

        this.choices = [];
    }

    addChoice(choice) {
        var nextId = this.choices.length;

        this.contents += "<button class='choice-btn' " +
            "id='" + this.name + "-choice-" + nextId + "'>" + choice.name +
            "</button>";

        this.choices.push(choice);

        return this;
    }

    write() {
        super.write();

        var popupBox = this;
        
        for (let i = 0; i < this.choices.length; i++) {
            $("#" + this.name + "-choice-" + i).click(function () {
                popupBox.hide();
                popupBox.choices[i].onChosen(popupBox.name);
            });
        }
    }

}

/*class YesNoPopupBox extends ChoicePopupBox {
    constructor(name, title, yes, no) {
        super(name, title, { yes, no });

        this.yesCallbacks = [];
        this.noCallbacks = [];

        this.yesNoCallbackList = [yesCallbacks, noCallbacks];

        addChoiceCallback(function () {
            yesNoCallbacks(function () {)
        });
    }
}*/

function makeComponentsChoosable(topNode, components,
        chosenCallback, unchosenCallback) {

    topNode.on("mouseenter", components, function () {
        if (!$(this).hasClass("option-chosen")) {
            $(this).removeClass("option-unselected")
                .addClass("option-selected");
        }

        if (!$(this).parent().hasClass("option-chosen")
            && $(this).parent().hasClass("option-selected")) {

            $(this).parent().removeClass("option-selected")
                .addClass("option-unselected");
        }
    });

    topNode.on("mouseleave", components, function () {
        if (!$(this).hasClass("option-chosen")) {
            $(this).removeClass("option-selected")
                .addClass("option-unselected");
        }

        if (!$(this).parent().hasClass("option-chosen")
            && $(this).parent().hasClass("option-unselected")) {

            $(this).parent().removeClass("option-unselected")
                .addClass("option-selected");
        }
    });

    topNode.on("click", components, function (e) {
        if ($(e.target).parents(".option-overlay").length) {
            return true;
        }

        if ($(this).hasClass("option-chosen")) {
            $(this).removeClass("option-chosen");

            unchosenCallback(this);
        } else {
            $(this).addClass("option-chosen");

            chosenCallback(this);
        }

        return true;
    });
}

function makeComponentsSelectable(topNode, components) {
    topNode.on("mouseenter", components, function () {
        $(this).removeClass("option-unselected").addClass("option-selected");

        if (!$(this).parent().hasClass("option-chosen")
            && $(this).parent().hasClass("option-selected")) {

            $(this).parent().removeClass("option-selected")
                .addClass("option-unselected");
        }
    });

    topNode.on("mouseleave", components, function () {
        $(this).removeClass("option-selected").addClass("option-unselected");

        if (!$(this).parent().hasClass("option-chosen")
            && $(this).parent().hasClass("option-unselected")) {

            $(this).parent().removeClass("option-unselected")
                .addClass("option-selected");
        }
    });
}

function dotText(component) {
    var originalText = component.text();
    var dots = 0;

    dotUpdate = function () {
        if (dots < MAX_DOTS) {
            component.append(".");
            dots++;
        } else {
            component.text(originalText);
            dots = 0;
        }
    }

    return setInterval(dotUpdate, DOTS_UPDATE_MILLISECONDS);
}

/*function makeComponentsChoosable(topNode, componentLevels) {
    var components = componentLevels[0];

    topNode.on("mouseenter", components, function () {
        if (!$(this).hasClass("option-chosen")) {
            $(this).removeClass("option-unselected").addClass("option-selected");
        }
    });

    topNode.on("mouseleave", components, function () {
        if (!$(this).hasClass("option-chosen")) {
            $(this).removeClass("option-selected").addClass("option-unselected");
        }
    });

    var len = componentLevels.length;
    var index;

    for (index = 1; index < len; index++) {
        components = componentLevels[index];

        topNode.on("mouseenter", components, function () {
            $(this).removeClass("option-unselected").addClass("option-selected");

            if (!$(this).parent().hasClass("option-chosen")) {
                $(this).parent().removeClass("option-selected")
                    .addClass("option-unselected");
            }
        });

        topNode.on("mouseleave", components, function () {
            $(this).removeClass("option-selected").addClass("option-unselected");

            if (!$(this).parent().hasClass("option-chosen")) {
                $(this).parent().removeClass("option-unselected")
                    .addClass("option-selected");
            }
        });
    }
}*/

/*function makeComponentsSelectable(topNode, componentLevels) {
    var components = componentLevels[0];

    topNode.on("mouseenter", components, function () {
        $(this).removeClass("option-unselected").addClass("option-selected");
    });

    topNode.on("mouseleave", components, function () {
        $(this).removeClass("option-selected").addClass("option-unselected");
    });

    var len = componentLevels.length;
    var index;

    for (index = 1; index < len; index++) {
        components = componentLevels[index];

        topNode.on("mouseenter", components, function () {
            $(this).removeClass("option-unselected").addClass("option-selected");

            $(this).parent().removeClass("option-selected")
                .addClass("option-unselected");
        });

        topNode.on("mouseleave", components, function () {
            $(this).removeClass("option-selected").addClass("option-unselected");

            $(this).parent().removeClass("option-unselected")
                .addClass("option-selected");
        });
    }
}*/