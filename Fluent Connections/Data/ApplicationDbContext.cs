﻿using System;
using System.Collections.Generic;
using System.Text;
using Fluent_Connections.Models.DataModels.Account;
using Fluent_Connections.Models.DataModels.IoT;
using Fluent_Connections.Models.DataModels.JoinModels;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Fluent_Connections.Data {
    public class ApplicationDbContext : IdentityDbContext<AppUser> {

        public ApplicationDbContext(
            DbContextOptions<ApplicationDbContext> options) : base(options) {

        }

        public DbSet<AppUser> AppUsers { get; set; }

        public DbSet<StandardUser> StandardUsers { get; set; }

        public DbSet<IoTSystem> IoTSystems { get; set; }

        public DbSet<ClientDevice> ClientDevices { get; set; }

        public DbSet<BeaconDevice> BeaconDevices { get; set; }

        public DbSet<TemperatureBeacon> TemperatureBeacon { get; set; }

        public DbSet<BeaconClientDevice> BeaconClientDevices { get; set; }

        public DbSet<StandardUserClientDevice> StandardUserClientDevices
            { get; set; }

        public DbSet<IoTBeaconDevice> IoTBeaconDevices { get; set; }

        public DbSet<UserBeaconDevice> UserBeaconDevices { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder) {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<StandardUserClientDevice>()
                .HasKey(sucd => new { sucd.StandardUserId,
                    sucd.ClientDeviceId });

            modelBuilder.Entity<BeaconClientDevice>().HasKey(
                bcd => new { bcd.BeaconDeviceId, bcd.ClientDeviceHashId });

            modelBuilder.Entity<UserBeaconDevice>().HasKey(
                ubd => new { ubd.StandardUserHashId, ubd.BeaconDeviceId });

            modelBuilder.Entity<IoTBeaconDevice>().HasKey(
                ibd => new { ibd.BeaconDeviceId, ibd.ClientDeviceHashId,
                    ibd.IoTSystemId, ibd.StandardUserHashId });

            /*modelBuilder.Entity<StandardUserClientBeaconDevice>().HasKey(
                sucbd => new { sucbd.StandardUserId, sucbd.ClientDeviceId,
                    sucbd.BeaconDeviceId });*/
        }

    }
}
