﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fluent_Connections {
    public class DeviceTypes {

        public static string GetTypeName(string modelNumber) {
            switch(modelNumber) {
                case "43234": return "Temperature";
                default: return "Unknown";
            }
        }

    }
}
