﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fluent_Connections {
    public class HashHelper {

        public static long LongHash(string str) {
            int half = str.Length / 2;
            long firstHash = str.Substring(0, half).GetHashCode();
            long secondHash = str.Substring(half).GetHashCode();

            return (firstHash << 32) | secondHash;
        }

    }
}
