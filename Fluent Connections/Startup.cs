﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Fluent_Connections.Data;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Fluent_Connections.Models.DataModels.Account;
using Fluent_Connections.StreamHubs;

namespace Fluent_Connections {
    public class Startup {

        private const int LOCKOUT_MINUTES = 15;
        private const int LOCKOUT_ATTEMPTS = 5;

        public Startup(IConfiguration configuration) {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services) {
            services.Configure<CookiePolicyOptions>(options => {
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(
                    Configuration.GetConnectionString("DefaultConnection")));

            LockoutOptions lockoutOptions = new LockoutOptions() {
                AllowedForNewUsers = true,
                DefaultLockoutTimeSpan = TimeSpan.FromMinutes(LOCKOUT_MINUTES),
                MaxFailedAccessAttempts = LOCKOUT_ATTEMPTS
            };

            services.AddIdentity<AppUser, IdentityRole>(options => {
                options.Lockout = lockoutOptions;
                options.Password.RequiredLength = 8;
                options.Password.RequireDigit = true;
                options.Password.RequireUppercase = true;
                options.Password.RequireLowercase = true;
                options.Password.RequireNonAlphanumeric = true;
            })
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            services.ConfigureApplicationCookie(
                options => options.LoginPath = "/Account/Login");

            services.AddMvc().SetCompatibilityVersion(
                CompatibilityVersion.Version_2_2);

            services.AddCors(options => options.AddPolicy("CorsPolicy", builder => {
                builder.AllowAnyMethod().AllowAnyHeader()
                    .WithOrigins("http://localhost:61138")
                    .AllowCredentials();
            }));

            services.AddSignalR();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env,
                ApplicationDbContext dbContext,
                UserManager<AppUser> userManager,
                SignInManager<AppUser> signInManager) {

            if (env.IsDevelopment()) {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            } else {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }

            /*AppDbInitialiser.AddSeededEntries(dbContext, userManager,
                signInManager).Wait();*/

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();

            app.UseAuthentication();

            app.UseCors("CorsPolicy");
            app.UseSignalR(routes => {
                routes.MapHub<DevicesHub>("/deviceshub");
            });

            app.UseMvc(routes => {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
