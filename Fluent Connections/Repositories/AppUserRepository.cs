﻿using Fluent_Connections.Data;
using Fluent_Connections.Models.DataModels.Account;
using Fluent_Connections.Models.DataModels.JoinModels;
using Fluent_Connections.Models.ViewModels.Account;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Fluent_Connections.Repositories {
    public class AppUserRepository {

        private readonly UserManager<AppUser> _userManager;
        private readonly SignInManager<AppUser> _signInManager;
        private readonly ApplicationDbContext _dbContext;

        public AppUserRepository(ApplicationDbContext dbContext,
                UserManager<AppUser> userManager,
                SignInManager<AppUser> signInManager) {

            this._dbContext = dbContext;
            this._userManager = userManager;
            this._signInManager = signInManager;
        }

        public async Task<SignInResult> SignInAsync(
                StandardLoginViewModel loginViewModel) {

            string username = loginViewModel.EmailAddress;
            string password = loginViewModel.Password;

            return await _signInManager.PasswordSignInAsync(username, password,
                false, true);
        }

        public async Task<SignInResult> SignInAsync(
                DeviceLoginViewModel loginViewModel) {

            string username = loginViewModel.Username;
            string password = loginViewModel.Password;

            return await _signInManager.PasswordSignInAsync(username, password,
                false, false);
        }

        public async Task SignInAsync(AppUser user) {
            await _signInManager.SignInAsync(user, false);
        }

        public async Task<bool> IsLockedAsync(string username) {
            AppUser user = await _userManager.FindByNameAsync(username);

            if(user == null) {
                return false;
            }

            return await _userManager.IsLockedOutAsync(user);
        }

        public async Task<IdentityResult> CreateUserAsync(AppUser user,
                string password) {

            IdentityResult result = await _userManager.CreateAsync(
                user, password);

            if(result.Succeeded) {
                user.HashId = user.Id.GetHashCode();
                result = await _userManager.UpdateAsync(user);;
            }

            return result;
        }

        public async Task<IdentityResult> UpdateUserAsync(AppUser user) {
            return await _userManager.UpdateAsync(user);
        }

        public async Task<AppUser> GetUserAsync(ClaimsPrincipal user) {
            AppUser appUser = await _userManager.GetUserAsync(user);

            if(appUser == null) {
                return null;
            }

            await appUser.Initialise(_dbContext);

            return appUser;
        }

        public async Task<AppUser> GetUserAsync(string username) {
            AppUser appUser = await _userManager.FindByNameAsync(username);

            if (appUser == null) {
                return null;
            }

            await appUser.Initialise(_dbContext);

            return appUser;
        }

        public async Task<AppUser> GetUserFromIdAsync(string id) {
            AppUser appUser = await _userManager.FindByIdAsync(id);

            if(appUser == null) {
                return null;
            }

            await appUser.Initialise(_dbContext);

            return appUser;
        }

        public async Task<bool> AddClientDeviceAsync(StandardUser standardUser,
                ClientDevice clientDevice) {

            bool exists = _dbContext.StandardUserClientDevices.Any(
                sucd => sucd.StandardUserId.Equals(standardUser.HashId)
                && sucd.ClientDeviceId.Equals(clientDevice.HashId));

            if (exists) {
                return false;
            }

            StandardUserClientDevice standardUserClientDevice =
                new StandardUserClientDevice(standardUser, clientDevice);

            await _dbContext.StandardUserClientDevices.AddAsync(
                standardUserClientDevice);

            await _dbContext.SaveChangesAsync();

            return true;
        }

        public async Task<bool> AddUserBeaconAsync(
                UserBeaconDevice userBeaconDevice) {

            long userId = userBeaconDevice.StandardUserHashId;
            long clientDeviceId = userBeaconDevice.ClientDeviceHashId;
            long beaconDeviceId = userBeaconDevice.BeaconDeviceId;

            bool exists = await
                _dbContext.UserBeaconDevices.AnyAsync(
                    ubd => ubd.StandardUserHashId.Equals(userId)
                && ubd.ClientDeviceHashId.Equals(clientDeviceId)
                && ubd.BeaconDeviceId.Equals(beaconDeviceId));

            if (exists) {
                return false;
            }

            await _dbContext.UserBeaconDevices.AddAsync(
                userBeaconDevice);

            await _dbContext.SaveChangesAsync();

            return true;
        }

        public async Task DeleteBeaconDeviceReferenceAsync(
            UserBeaconDevice userBeaconDevice) {

            _dbContext.Remove(userBeaconDevice);

            await _dbContext.SaveChangesAsync();
        }

        public async Task DeleteBeaconDeviceReferencesAsync(
                StandardUser standardUser, long[] ids) {

            List<UserBeaconDevice> userBeaconDevices =
                await _dbContext.UserBeaconDevices.Where(ubd =>
                ubd.StandardUserHashId.Equals(standardUser.HashId) &&
                ids.Any(id => id.Equals(ubd.BeaconDeviceId))).ToListAsync();

            foreach (UserBeaconDevice userBeaconDevice in userBeaconDevices) {

                _dbContext.UserBeaconDevices.Remove(userBeaconDevice);
            }

            await _dbContext.SaveChangesAsync();
        }

        public async Task SignOut() {
            await _signInManager.SignOutAsync();
        }

    }
}
