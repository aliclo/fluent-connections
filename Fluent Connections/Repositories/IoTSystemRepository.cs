﻿using Fluent_Connections.Data;
using Fluent_Connections.Models.DataModels.Account;
using Fluent_Connections.Models.DataModels.IoT;
using Fluent_Connections.Models.DataModels.JoinModels;
using Fluent_Connections.Models.ViewModels.IoT;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fluent_Connections.Repositories {
    public class IoTSystemRepository {

        private readonly ApplicationDbContext _dbContext;

        public IoTSystemRepository(ApplicationDbContext dbContext) {
            this._dbContext = dbContext;
        }

        public async Task AddIoTSystemAsync(IoTSystem iotSystem) {
            await _dbContext.IoTSystems.AddAsync(iotSystem);
            await _dbContext.SaveChangesAsync();
        }

        public async Task DeleteIoTSystemAsync(IoTSystem iotSystem) {
            _dbContext.IoTSystems.Remove(iotSystem);
            await _dbContext.SaveChangesAsync();
        }

        public async Task<List<IoTSystem>> GetIoTSystemsAsync(AppUser user) {
            return await _dbContext.IoTSystems.Where(
                i => i.User.Id.Equals(user.Id)).ToListAsync();
        }

        public async Task<IoTSystem> GetIoTSystemAsync(string id) {
            return await _dbContext.IoTSystems.SingleAsync(
                i => i.Id.Equals(id));
        }

        public async Task<bool> AddBeaconDeviceAsync(
                BeaconDevice beaconDevice) {

            bool exists = await _dbContext.BeaconDevices.AnyAsync(b =>
                b.ModelNumber.Equals(beaconDevice.ModelNumber) &&
                b.SerialNumber.Equals(beaconDevice.SerialNumber));

            if(exists) {
                return false;
            }

            await _dbContext.BeaconDevices.AddAsync(beaconDevice);
            await _dbContext.SaveChangesAsync();

            return true;
        }

        public async Task DeleteBeaconDeviceAsync(BeaconDevice beaconDevice) {
            _dbContext.BeaconDevices.Remove(beaconDevice);
            await _dbContext.SaveChangesAsync();
        }

        public async Task UpdateUserBeaconDeviceAsync(
                UserBeaconDevice userBeaconDevice) {

            _dbContext.UserBeaconDevices.Update(userBeaconDevice);
            await _dbContext.SaveChangesAsync();
        }

        public async Task<List<UserBeaconDevice>> GetBeaconDevicesAsync(
                AppUser user) {

            List<UserBeaconDevice> userBeaconDevices =
                await _dbContext.UserBeaconDevices.Where(ubd =>
                ubd.StandardUserHashId.Equals(user.HashId)).ToListAsync();

            foreach(UserBeaconDevice userBeaconDevice in userBeaconDevices) {
                await userBeaconDevice.Initialise(_dbContext);
            }

            return userBeaconDevices;
        }

        public async Task<List<UserBeaconDevice>> GetNonIoTBeaconDevicesAsync(
                AppUser user, string iotSystemId) {

            IQueryable<IoTBeaconDevice> iotBeaconDevices =
                _dbContext.IoTBeaconDevices.Where(ibd =>
                ibd.IoTSystemId.Equals(iotSystemId));

            IQueryable<UserBeaconDevice> userBeaconDevices =
                _dbContext.UserBeaconDevices.Where(ubd =>
                ubd.StandardUserHashId.Equals(user.HashId));

            List<UserBeaconDevice> nonIoTBeaconDevices = await
                userBeaconDevices.Where(ubd =>
                !iotBeaconDevices.Any(ibd =>
                ibd.BeaconDeviceId.Equals(ubd.BeaconDeviceId))).ToListAsync();

            foreach (UserBeaconDevice userBeaconDevice in userBeaconDevices) {
                await userBeaconDevice.Initialise(_dbContext);
            }

            return nonIoTBeaconDevices;
        }

        public async Task<UserBeaconDevice> GetBeaconDeviceAsync(
                StandardUser user, long beaconId) {

            UserBeaconDevice userBeaconDevice =
                await _dbContext.UserBeaconDevices.SingleAsync(
                b => b.StandardUserHashId == user.HashId
                && b.BeaconDeviceId.Equals(beaconId));

            await userBeaconDevice.Initialise(_dbContext);

            return userBeaconDevice;
        }

        public async Task<UserBeaconDevice[]> GetBeaconDevicesAsync(
                UserBeaconDeviceIDViewModel[] ids) {

            UserBeaconDevice[] userBeaconDevices = await _dbContext
                .UserBeaconDevices.Where(b => ids.Any(id =>
                b.StandardUserHashId.Equals(id.StandardUserHashId) &&
                b.ClientDeviceHashId.Equals(id.ClientDeviceHashId) &&
                b.BeaconDeviceId.Equals(id.BeaconDeviceId))).ToArrayAsync();

            foreach(UserBeaconDevice userBeaconDevice in userBeaconDevices) {
                await userBeaconDevice.Initialise(_dbContext);
            }

            return userBeaconDevices;
        }

        public async Task<string[]> GetBeaconWidgetsAsync(
                IBeaconDeviceId[] ids) {

            string[] widgets = await _dbContext.BeaconDevices
                .Where(b =>ids.Any(id => b.Id.Equals(id.BeaconDeviceId)))
                .Select(b => b.GetHtmlDisplay()).ToArrayAsync();

            return widgets;
        }

        public async Task<bool> HasBeaconDeviceAsync(
                AppUser user, string address) {

            IQueryable<UserBeaconDevice> userBeacons =
                _dbContext.UserBeaconDevices.Where(ub =>
                ub.StandardUserHashId.Equals(user.HashId));

            return await _dbContext.BeaconDevices.AnyAsync(b =>
                userBeacons.Any(ub => ub.BeaconDeviceId.Equals(b.Id)) &&
                b.Address.Equals(address));
        }

        /*public async Task<List<UserBeaconDevice>> GetBeaconDevicesAsync(
                IBeaconId[] beaconIds) {

            _dbContext.UserBeac
        }*/

        public async Task<bool> AddIoTBeaconDevicesAsync(AppUser user,
                IBeaconId[] beaconIds, string iotSystemId) {

            bool userOwnsAll = beaconIds.All(
                d => d.StandardUserHashId.Equals(user.HashId));

            if(!userOwnsAll) {
                return false;
            }

            //Check that beacons from given ids exist while retrieving them
            List<UserBeaconDevice> userBeaconDevices = await
                _dbContext.UserBeaconDevices.Where(ubd => beaconIds.Any(bi =>
                ubd.StandardUserHashId.Equals(bi.StandardUserHashId) &&
                ubd.BeaconDeviceId.Equals(bi.BeaconDeviceId) &&
                ubd.ClientDeviceHashId.Equals(bi.ClientDeviceHashId)))
                .ToListAsync();

            IoTSystem iotSystem = await _dbContext.IoTSystems.SingleAsync(
                i => i.Id.Equals(iotSystemId));

            foreach(UserBeaconDevice userBeaconDevice in userBeaconDevices) {
                IoTBeaconDevice iotBeaconDevice = new IoTBeaconDevice(
                    userBeaconDevice, iotSystem);

                await _dbContext.IoTBeaconDevices.AddAsync(iotBeaconDevice);
            }

            await _dbContext.SaveChangesAsync();

            return true;
        }

        public async Task<bool> RemoveIoTBeaconDevicesAsync(AppUser user,
                IBeaconId[] beaconIds, string iotSystemId) {

            bool userOwnsAll = beaconIds.All(
                d => d.StandardUserHashId.Equals(user.HashId));

            if (!userOwnsAll) {
                return false;
            }

            foreach (IBeaconId beacon in beaconIds) {
                IoTBeaconDevice iotBeaconDevice = new IoTBeaconDevice(
                    beacon, iotSystemId);

                _dbContext.IoTBeaconDevices.Attach(iotBeaconDevice);
                _dbContext.IoTBeaconDevices.Remove(iotBeaconDevice);
            }

            await _dbContext.SaveChangesAsync();

            return true;
        }

        public async Task AddBeaconClientDeviceAsync(
                BeaconClientDevice beaconClientDevice) {

            await _dbContext.BeaconClientDevices.AddAsync(beaconClientDevice);
            await _dbContext.SaveChangesAsync();
        }

        public async Task AddIoTBeaconDeviceAsync (
                IoTBeaconDevice iotBeaconDevice) {

            await _dbContext.IoTBeaconDevices.AddAsync(iotBeaconDevice);
            await _dbContext.SaveChangesAsync();
        }

        public async Task<List<IoTBeaconDevice>> GetIoTBeaconDeivcesAsync(
                StandardUser standardUser, string iotSystemId) {

            List<IoTBeaconDevice> iotBeaconDevices =
                await _dbContext.IoTBeaconDevices.Where(b =>
                b.StandardUserHashId.Equals(standardUser.HashId) &&
                b.IoTSystemId.Equals(iotSystemId)).ToListAsync();

            return iotBeaconDevices;
        }

    }
}
