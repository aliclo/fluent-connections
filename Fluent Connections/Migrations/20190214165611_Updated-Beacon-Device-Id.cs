﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Fluent_Connections.Migrations
{
    public partial class UpdatedBeaconDeviceId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "HashId",
                table: "BeaconDevices",
                newName: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Id",
                table: "BeaconDevices",
                newName: "HashId");
        }
    }
}
