﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Fluent_Connections.Migrations
{
    public partial class updateduserbeaconclientdevicerelations : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "StandardUserClientBeaconDevice");

            migrationBuilder.RenameColumn(
                name: "Type",
                table: "BeaconDevices",
                newName: "SerialNumber");

            migrationBuilder.RenameColumn(
                name: "Description",
                table: "BeaconDevices",
                newName: "ModelNumber");

            migrationBuilder.AddColumn<string>(
                name: "BluetoothAddress",
                table: "BeaconDevices",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "BeaconClientDevices",
                columns: table => new
                {
                    BeaconDeviceId = table.Column<long>(nullable: false),
                    ClientDeviceHashId = table.Column<long>(nullable: false),
                    ClientDeviceId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BeaconClientDevices", x => new { x.BeaconDeviceId, x.ClientDeviceHashId });
                    table.ForeignKey(
                        name: "FK_BeaconClientDevices_BeaconDevices_BeaconDeviceId",
                        column: x => x.BeaconDeviceId,
                        principalTable: "BeaconDevices",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BeaconClientDevices_AspNetUsers_ClientDeviceId",
                        column: x => x.ClientDeviceId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UserBeaconDevices",
                columns: table => new
                {
                    StandardUserHashId = table.Column<long>(nullable: false),
                    BeaconDeviceId = table.Column<long>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    StandardUserId = table.Column<string>(nullable: true),
                    ClientDeviceHashId = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserBeaconDevices", x => new { x.StandardUserHashId, x.BeaconDeviceId });
                    table.ForeignKey(
                        name: "FK_UserBeaconDevices_AspNetUsers_StandardUserId",
                        column: x => x.StandardUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UserBeaconDevices_BeaconClientDevices_BeaconDeviceId_ClientDeviceHashId",
                        columns: x => new { x.BeaconDeviceId, x.ClientDeviceHashId },
                        principalTable: "BeaconClientDevices",
                        principalColumns: new[] { "BeaconDeviceId", "ClientDeviceHashId" },
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BeaconClientDevices_ClientDeviceId",
                table: "BeaconClientDevices",
                column: "ClientDeviceId");

            migrationBuilder.CreateIndex(
                name: "IX_UserBeaconDevices_StandardUserId",
                table: "UserBeaconDevices",
                column: "StandardUserId");

            migrationBuilder.CreateIndex(
                name: "IX_UserBeaconDevices_BeaconDeviceId_ClientDeviceHashId",
                table: "UserBeaconDevices",
                columns: new[] { "BeaconDeviceId", "ClientDeviceHashId" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "UserBeaconDevices");

            migrationBuilder.DropTable(
                name: "BeaconClientDevices");

            migrationBuilder.DropColumn(
                name: "BluetoothAddress",
                table: "BeaconDevices");

            migrationBuilder.RenameColumn(
                name: "SerialNumber",
                table: "BeaconDevices",
                newName: "Type");

            migrationBuilder.RenameColumn(
                name: "ModelNumber",
                table: "BeaconDevices",
                newName: "Description");

            migrationBuilder.CreateTable(
                name: "StandardUserClientBeaconDevice",
                columns: table => new
                {
                    StandardUserId = table.Column<long>(nullable: false),
                    ClientDeviceId = table.Column<long>(nullable: false),
                    BeaconDeviceId = table.Column<long>(nullable: false),
                    ClientDeviceId1 = table.Column<string>(nullable: true),
                    StandardUserId1 = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StandardUserClientBeaconDevice", x => new { x.StandardUserId, x.ClientDeviceId, x.BeaconDeviceId });
                    table.ForeignKey(
                        name: "FK_StandardUserClientBeaconDevice_BeaconDevices_BeaconDeviceId",
                        column: x => x.BeaconDeviceId,
                        principalTable: "BeaconDevices",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_StandardUserClientBeaconDevice_AspNetUsers_ClientDeviceId1",
                        column: x => x.ClientDeviceId1,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_StandardUserClientBeaconDevice_AspNetUsers_StandardUserId1",
                        column: x => x.StandardUserId1,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_StandardUserClientBeaconDevice_BeaconDeviceId",
                table: "StandardUserClientBeaconDevice",
                column: "BeaconDeviceId");

            migrationBuilder.CreateIndex(
                name: "IX_StandardUserClientBeaconDevice_ClientDeviceId1",
                table: "StandardUserClientBeaconDevice",
                column: "ClientDeviceId1");

            migrationBuilder.CreateIndex(
                name: "IX_StandardUserClientBeaconDevice_StandardUserId1",
                table: "StandardUserClientBeaconDevice",
                column: "StandardUserId1");
        }
    }
}
