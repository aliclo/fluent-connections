﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Fluent_Connections.Migrations
{
    public partial class addedIoTBeaconDevice : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "IoTBeaconDevices",
                columns: table => new
                {
                    StandardUserHashId = table.Column<long>(nullable: false),
                    BeaconDeviceId = table.Column<long>(nullable: false),
                    ClientDeviceHashId = table.Column<long>(nullable: false),
                    IoTSystemId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IoTBeaconDevices", x => new { x.BeaconDeviceId, x.ClientDeviceHashId, x.IoTSystemId, x.StandardUserHashId });
                    table.ForeignKey(
                        name: "FK_IoTBeaconDevices_IoTSystems_IoTSystemId",
                        column: x => x.IoTSystemId,
                        principalTable: "IoTSystems",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_IoTBeaconDevices_UserBeaconDevices_StandardUserHashId_BeaconDeviceId",
                        columns: x => new { x.StandardUserHashId, x.BeaconDeviceId },
                        principalTable: "UserBeaconDevices",
                        principalColumns: new[] { "StandardUserHashId", "BeaconDeviceId" },
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_IoTBeaconDevices_IoTSystemId",
                table: "IoTBeaconDevices",
                column: "IoTSystemId");

            migrationBuilder.CreateIndex(
                name: "IX_IoTBeaconDevices_StandardUserHashId_BeaconDeviceId",
                table: "IoTBeaconDevices",
                columns: new[] { "StandardUserHashId", "BeaconDeviceId" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "IoTBeaconDevices");
        }
    }
}
