﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Fluent_Connections.Migrations
{
    public partial class addedtemperaturebeacon : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Discriminator",
                table: "BeaconDevices",
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Discriminator",
                table: "BeaconDevices");
        }
    }
}
