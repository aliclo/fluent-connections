﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Fluent_Connections.Migrations
{
    public partial class addedBeaconDevices : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AspNetUsers_AspNetUsers_ClientDeviceId",
                table: "AspNetUsers");

            migrationBuilder.DropIndex(
                name: "IX_AspNetUsers_ClientDeviceId",
                table: "AspNetUsers");

            migrationBuilder.RenameColumn(
                name: "ClientDeviceId",
                table: "AspNetUsers",
                newName: "Type");

            migrationBuilder.AlterColumn<string>(
                name: "Type",
                table: "AspNetUsers",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.CreateTable(
                name: "StandardUserClientBeaconDevice",
                columns: table => new
                {
                    StandardUserId = table.Column<long>(nullable: false),
                    ClientDeviceId = table.Column<long>(nullable: false),
                    BeaconDeviceId = table.Column<long>(nullable: false),
                    StandardUserId1 = table.Column<string>(nullable: true),
                    ClientDeviceId1 = table.Column<string>(nullable: true),
                    BeaconDeviceId1 = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StandardUserClientBeaconDevice", x => new { x.StandardUserId, x.ClientDeviceId, x.BeaconDeviceId });
                    table.ForeignKey(
                        name: "FK_StandardUserClientBeaconDevice_AspNetUsers_BeaconDeviceId1",
                        column: x => x.BeaconDeviceId1,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_StandardUserClientBeaconDevice_AspNetUsers_ClientDeviceId1",
                        column: x => x.ClientDeviceId1,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_StandardUserClientBeaconDevice_AspNetUsers_StandardUserId1",
                        column: x => x.StandardUserId1,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_StandardUserClientBeaconDevice_BeaconDeviceId1",
                table: "StandardUserClientBeaconDevice",
                column: "BeaconDeviceId1");

            migrationBuilder.CreateIndex(
                name: "IX_StandardUserClientBeaconDevice_ClientDeviceId1",
                table: "StandardUserClientBeaconDevice",
                column: "ClientDeviceId1");

            migrationBuilder.CreateIndex(
                name: "IX_StandardUserClientBeaconDevice_StandardUserId1",
                table: "StandardUserClientBeaconDevice",
                column: "StandardUserId1");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "StandardUserClientBeaconDevice");

            migrationBuilder.RenameColumn(
                name: "Type",
                table: "AspNetUsers",
                newName: "ClientDeviceId");

            migrationBuilder.AlterColumn<string>(
                name: "ClientDeviceId",
                table: "AspNetUsers",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUsers_ClientDeviceId",
                table: "AspNetUsers",
                column: "ClientDeviceId");

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUsers_AspNetUsers_ClientDeviceId",
                table: "AspNetUsers",
                column: "ClientDeviceId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
