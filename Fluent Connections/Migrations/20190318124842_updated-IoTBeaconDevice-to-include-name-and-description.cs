﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Fluent_Connections.Migrations
{
    public partial class updatedIoTBeaconDevicetoincludenameanddescription : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "IoTBeaconDevices",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "IoTBeaconDevices",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Description",
                table: "IoTBeaconDevices");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "IoTBeaconDevices");
        }
    }
}
