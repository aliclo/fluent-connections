﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Fluent_Connections.Migrations
{
    public partial class addedbtaddresstodevice : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Address",
                table: "BeaconDevices",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Address",
                table: "BeaconDevices");
        }
    }
}
