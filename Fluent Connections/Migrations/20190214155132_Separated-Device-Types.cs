﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Fluent_Connections.Migrations
{
    public partial class SeparatedDeviceTypes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_StandardUserClientBeaconDevice_AspNetUsers_BeaconDeviceId1",
                table: "StandardUserClientBeaconDevice");

            migrationBuilder.DropIndex(
                name: "IX_StandardUserClientBeaconDevice_BeaconDeviceId1",
                table: "StandardUserClientBeaconDevice");

            migrationBuilder.DropColumn(
                name: "BeaconDeviceId1",
                table: "StandardUserClientBeaconDevice");

            migrationBuilder.DropColumn(
                name: "Type",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "Desc",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "AspNetUsers");

            migrationBuilder.CreateTable(
                name: "BeaconDevices",
                columns: table => new
                {
                    HashId = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Type = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BeaconDevices", x => x.HashId);
                });

            migrationBuilder.CreateIndex(
                name: "IX_StandardUserClientBeaconDevice_BeaconDeviceId",
                table: "StandardUserClientBeaconDevice",
                column: "BeaconDeviceId");

            migrationBuilder.AddForeignKey(
                name: "FK_StandardUserClientBeaconDevice_BeaconDevices_BeaconDeviceId",
                table: "StandardUserClientBeaconDevice",
                column: "BeaconDeviceId",
                principalTable: "BeaconDevices",
                principalColumn: "HashId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_StandardUserClientBeaconDevice_BeaconDevices_BeaconDeviceId",
                table: "StandardUserClientBeaconDevice");

            migrationBuilder.DropTable(
                name: "BeaconDevices");

            migrationBuilder.DropIndex(
                name: "IX_StandardUserClientBeaconDevice_BeaconDeviceId",
                table: "StandardUserClientBeaconDevice");

            migrationBuilder.AddColumn<string>(
                name: "BeaconDeviceId1",
                table: "StandardUserClientBeaconDevice",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Type",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Desc",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_StandardUserClientBeaconDevice_BeaconDeviceId1",
                table: "StandardUserClientBeaconDevice",
                column: "BeaconDeviceId1");

            migrationBuilder.AddForeignKey(
                name: "FK_StandardUserClientBeaconDevice_AspNetUsers_BeaconDeviceId1",
                table: "StandardUserClientBeaconDevice",
                column: "BeaconDeviceId1",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
