﻿using Fluent_Connections.Authorization;
using Fluent_Connections.Data;
using Fluent_Connections.Models.DataModels.Account;
using Fluent_Connections.Models.ViewModels.Account;
using Fluent_Connections.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using SignInResult = Microsoft.AspNetCore.Identity.SignInResult;

namespace Fluent_Connections.Controllers {

    [Authorize]
    public class AccountController : Controller {

        private readonly AppUserRepository _appUserRepository;

        public AccountController(ApplicationDbContext dbContext,
                UserManager<AppUser> userManager,
                SignInManager<AppUser> signInManager) {

            _appUserRepository = new AppUserRepository(dbContext, userManager,
                signInManager);
        }

        [AllowAnonymous]
        [OnlyAnonymous]
        [HttpGet]
        public IActionResult Login() {
            return View();
        }

        [AllowAnonymous]
        [OnlyAnonymous]
        [HttpPost]
        public async Task<IActionResult> Login(
                StandardLoginViewModel standardLoginViewModel) {

            if(!ModelState.IsValid) {
                return View(standardLoginViewModel);
            }

            bool lockedOut = await _appUserRepository.IsLockedAsync(
                standardLoginViewModel.EmailAddress);

            if(lockedOut) {
                ModelState.AddModelError("", "Account is locked");
                return View(standardLoginViewModel);
            }

            SignInResult result = await _appUserRepository.SignInAsync(
                standardLoginViewModel);

            if (!result.Succeeded) {
                ModelState.AddModelError("", "Failed to login");
                return View(standardLoginViewModel);
            }

            return RedirectToAction("Index", "Home");
        }

        [AllowAnonymous]
        [OnlyAnonymous]
        [HttpGet]
        public IActionResult LoginDevice() {
            return View();
        }

        [AllowAnonymous]
        [OnlyAnonymous]
        [HttpPost]
        public async Task<IActionResult> LoginDevice(
                DeviceLoginViewModel deviceLoginViewModel) {

            if (!ModelState.IsValid) {
                return View(deviceLoginViewModel);
            }

            bool lockedOut = await _appUserRepository.IsLockedAsync(
                deviceLoginViewModel.Username);

            if (lockedOut) {
                ModelState.AddModelError("", "Account is locked");
                return View(deviceLoginViewModel);
            }

            SignInResult result = await _appUserRepository.SignInAsync(
                deviceLoginViewModel);

            if (!result.Succeeded) {
                ModelState.AddModelError("", "Failed to login");
                return View(deviceLoginViewModel);
            }

            return RedirectToAction("Index", "Home");
        }

        [AllowAnonymous]
        [OnlyAnonymous]
        [HttpPost]
        public async Task<IActionResult> AuthenticateDevice(
                DeviceLoginViewModel deviceLoginViewModel) {

            bool lockedOut = await _appUserRepository.IsLockedAsync(
               deviceLoginViewModel.Username);

            if (lockedOut) {
                return BadRequest();
            }

            SignInResult result = await _appUserRepository.SignInAsync(
                deviceLoginViewModel);

            if (!result.Succeeded) {
                return BadRequest();
            }

            return Ok();
        }

        [HttpPost]
        public async Task<IActionResult> UpdateIpAddress(string ipAddress) {
            try {
                ClientDevice clientDevice = (ClientDevice)await
                    _appUserRepository.GetUserAsync(User);

                clientDevice.IpAddress = ipAddress;

                await _appUserRepository.UpdateUserAsync(clientDevice);
            } catch (InvalidCastException) {
                return BadRequest();
            }

            return Ok();
        }

        [AllowAnonymous]
        [OnlyAnonymous]
        [HttpGet]
        public IActionResult StandardRegister() {
            return View();
        }

        [AllowAnonymous]
        [OnlyAnonymous]
        [HttpPost]
        public async Task<IActionResult> StandardRegister(
                StandardRegisterViewModel registerViewModel) {

            if (!ModelState.IsValid) {
                return View(registerViewModel);
            }

            StandardUser standardUser = new StandardUser(registerViewModel);

            IdentityResult result = await _appUserRepository.CreateUserAsync(
                standardUser, registerViewModel.Password);

            if(!result.Succeeded) {
                foreach(IdentityError error in result.Errors) {
                    ModelState.AddModelError("", error.Description);
                }

                return View(registerViewModel);
            }

            await _appUserRepository.SignInAsync(standardUser);

            return RedirectToAction("Index", "Home");
        }

        [AllowAnonymous]
        [OnlyAnonymous]
        [HttpPost]
        public async Task<IActionResult> AddClientDevice(
                DeviceRegisterViewModel deviceRegisterViewModel) {

            ClientDevice clientDevice = new ClientDevice(
                deviceRegisterViewModel);

            IdentityResult result = await _appUserRepository.CreateUserAsync(
                clientDevice, deviceRegisterViewModel.Password);

            if (!result.Succeeded) {
                return BadRequest();
            }

            await _appUserRepository.SignInAsync(clientDevice);

            return Ok();
        }

        [HttpGet]
        public IActionResult CheckSignedIn() {
            return Ok();
        }

        public async Task<IActionResult> Logout() {
            await _appUserRepository.SignOut();

            return RedirectToAction("Login");
        }

    }
}