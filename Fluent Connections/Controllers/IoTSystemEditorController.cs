﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Fluent_Connections.Data;
using Fluent_Connections.Models.DataModels.Account;
using Fluent_Connections.Models.DataModels.JoinModels;
using Fluent_Connections.Models.ViewModels.IoT;
using Fluent_Connections.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace Fluent_Connections.Controllers
{
    [Authorize]
    public class IoTSystemEditorController : Controller {

        private readonly AppUserRepository _appUserRepository;
        private readonly IoTSystemRepository _iotSystemRepository;

        public IoTSystemEditorController(ApplicationDbContext dbContext,
                UserManager<AppUser> userManager,
                SignInManager<AppUser> signInManager) {

            _appUserRepository = new AppUserRepository(dbContext, userManager,
                signInManager);

            _iotSystemRepository = new IoTSystemRepository(dbContext);
        }

        public async Task<IActionResult> Index(string id) {
            StandardUser user = (StandardUser)
                await _appUserRepository.GetUserAsync(User);

            List<IoTBeaconDevice> iotBeaconDevices =
                await _iotSystemRepository.GetIoTBeaconDeivcesAsync(user, id);

            List<UserBeaconDevice> userBeaconDevices =
                await _iotSystemRepository.GetNonIoTBeaconDevicesAsync(
                    user, id);

            IoTSystemDevicesViewModel iotSystemDevicesViewModel =
                new IoTSystemDevicesViewModel(id, iotBeaconDevices,
                userBeaconDevices);

            return View(iotSystemDevicesViewModel);
        }

        public async Task<IActionResult> AddIoTSystemDevices(
                string iotSystemId, UserBeaconDeviceIDViewModel[] devices) {

            StandardUser user = (StandardUser) await _appUserRepository
                .GetUserAsync(User);

            bool success = await _iotSystemRepository
                .AddIoTBeaconDevicesAsync(user, devices, iotSystemId);

            if (success) {
                string[] widgetsHtml = await _iotSystemRepository
                    .GetBeaconWidgetsAsync(devices);

                return Json(widgetsHtml);
            } else {
                return BadRequest();
            }
        }

        public async Task<IActionResult> RemoveIoTSystemDevices(
                string iotSystemId, UserBeaconDeviceIDViewModel[] devices) {

            AppUser user = await _appUserRepository.GetUserAsync(User);

            bool success = await _iotSystemRepository
                .RemoveIoTBeaconDevicesAsync(user, devices, iotSystemId);

            if (success) {
                return Ok();
            } else {
                return BadRequest();
            }
        }
    }
}