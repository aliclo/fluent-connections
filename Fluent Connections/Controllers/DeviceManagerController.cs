﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Fluent_Connections.Data;
using Fluent_Connections.Models.DataModels.Account;
using Fluent_Connections.Models.DataModels.JoinModels;
using Fluent_Connections.Models.ViewModels.IoT;
using Fluent_Connections.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace Fluent_Connections.Controllers
{

    [Authorize]
    public class DeviceManagerController : Controller {

        private readonly ApplicationDbContext _dbContext;
        private readonly AppUserRepository _appUserRepository;
        private readonly IoTSystemRepository _iotSystemRepository;
        private readonly HttpClient _client = new HttpClient();

        public DeviceManagerController(ApplicationDbContext dbContext,
                UserManager<AppUser> userManager,
                SignInManager<AppUser> signInManager) {

            _dbContext = dbContext;

            _appUserRepository = new AppUserRepository(dbContext,
                userManager, signInManager);

            _iotSystemRepository = new IoTSystemRepository(dbContext);
        }

        public IActionResult Index() {
            /*StandardUser user = (StandardUser) await _appUserRepository.GetUserAsync(User);

            user.Initialise();

            user.ClientDevices.get*/

            return View();
        }

        public async Task<IActionResult> AddBluetoothDevices(
                [FromBody] SelectedBeaconDeviceViewModel[]
                selectedBeaconDevices) {

            //NEED TO HANDLE WHEN USER IS NOT STANDARD USER
            StandardUser standardUser = (StandardUser)
                await _appUserRepository.GetUserAsync(User);

            foreach(SelectedBeaconDeviceViewModel selectedBeaconDevice
                    in selectedBeaconDevices) {

                BeaconDevice beaconDevice = BeaconDevice.CreateBeaconDevice(
                    selectedBeaconDevice);

                await _iotSystemRepository.AddBeaconDeviceAsync(
                    beaconDevice);

                string clientDeviceId = selectedBeaconDevice.ClientDeviceId;

                ClientDevice clientDevice = (ClientDevice) await
                    _appUserRepository.GetUserFromIdAsync(clientDeviceId);

                BeaconClientDevice beaconClientDevice =
                    new BeaconClientDevice(clientDevice, beaconDevice);

                await _iotSystemRepository.AddBeaconClientDeviceAsync(
                    beaconClientDevice);

                UserBeaconDevice userBeaconDevice = new UserBeaconDevice(
                    beaconDevice.Name, "", standardUser, beaconClientDevice);

                await _appUserRepository.AddUserBeaconAsync(userBeaconDevice);
            }

            return Ok();
        }

    }
}