﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Fluent_Connections.Models;
using Microsoft.AspNetCore.Authorization;
using Fluent_Connections.Data;
using Fluent_Connections.Repositories;
using Microsoft.AspNetCore.Identity;
using Fluent_Connections.Models.DataModels.Account;
using Fluent_Connections.Models.DataModels.IoT;
using Fluent_Connections.Models.ViewModels.IoT;
using Fluent_Connections.Models.ViewModels.Account;
using Fluent_Connections.Models.DataModels.JoinModels;

namespace Fluent_Connections.Controllers {

    [Authorize]
    public class HomeController : Controller {

        private readonly AppUserRepository _appUserRepository;
        private readonly IoTSystemRepository _iotSystemRepository;

        public HomeController(ApplicationDbContext dbContext,
                UserManager<AppUser> userManager,
                SignInManager<AppUser> signInManager) {

            _appUserRepository = new AppUserRepository(dbContext,
                userManager, signInManager);

            _iotSystemRepository = new IoTSystemRepository(dbContext);
        }

        public async Task<IActionResult> Index() {
            AppUser user = await _appUserRepository.GetUserAsync(User);

            return await user.GetHomePage(this, _iotSystemRepository);
        }

        public async Task<IActionResult> AddIoTSystem(string name,
                string description) {

            StandardUser standardUser = (StandardUser) await
                _appUserRepository.GetUserAsync(User);

            IoTSystem iotSystem = new IoTSystem(name, description,
                standardUser);

            await _iotSystemRepository.AddIoTSystemAsync(iotSystem);

            AddedIoTSystemViewModel addedIoTSystem =
                new AddedIoTSystemViewModel(iotSystem);

            return Json(addedIoTSystem);
        }

        public async Task<IActionResult> DeleteIoTSystem(string id) {
            IoTSystem iotSystem = await _iotSystemRepository
                .GetIoTSystemAsync(id);

            await _iotSystemRepository.DeleteIoTSystemAsync(iotSystem);

            return NoContent();
        }

        public async Task<IActionResult> DeleteDevices(long[] deviceIds) {
            StandardUser standardUser = (StandardUser) await
                _appUserRepository.GetUserAsync(User);

            await _appUserRepository.DeleteBeaconDeviceReferencesAsync(
                standardUser, deviceIds);

            return Ok();
        }

        public async Task<IActionResult> EditIoTSystem(
                ModifiedBeaconDeviceViewModel modifiedBeaconDevice) {

            //NEED TO HANDLE WHEN USER IS NOT STANDARD USER
            StandardUser standardUser = (StandardUser)
                await _appUserRepository.GetUserAsync(User);

            long id = modifiedBeaconDevice.Id;

            UserBeaconDevice beaconDevice = await
                _iotSystemRepository.GetBeaconDeviceAsync(standardUser, id);

            beaconDevice.UpdateDetails(modifiedBeaconDevice);

            await _iotSystemRepository.UpdateUserBeaconDeviceAsync(
                beaconDevice);

            return NoContent();
        }

        public async Task<IActionResult> AddToAccount(
                AddedAccountViewModel addedAccount) {

            ClientDevice clientDevice =
                (ClientDevice) await _appUserRepository.GetUserAsync(User);

            string userName = addedAccount.EmailAddress;

            AppUser user = await _appUserRepository.GetUserAsync(userName);

            if(user == null) {
                ModelState.AddModelError("", "User does not exist");
                return View("DeviceHome", addedAccount);
            }

            bool success = await user.AddDevice(clientDevice,
                _appUserRepository);

            if(success) {
                return RedirectToAction("Index");
            } else {
                ModelState.AddModelError("", "Failed to add to account");
                return View("DeviceHome", addedAccount);
            }
        }

        public IActionResult IoTSystemEditor(string id) {
            return RedirectToAction("IoTSystemEditor", "Index",
                new { id });
        }

        public IActionResult DeviceManager() {
            return RedirectToAction("DeviceManager", "Index");
        }

        public IActionResult About() {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact() {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy() {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error() {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
