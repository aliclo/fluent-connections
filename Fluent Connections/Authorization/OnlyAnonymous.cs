﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fluent_Connections.Authorization {
    public class OnlyAnonymous : TypeFilterAttribute {

        public OnlyAnonymous() : base(typeof(OnlyAnonymousFilter)) {

        }

    }
}
