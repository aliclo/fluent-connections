﻿using Fluent_Connections.Data;
using Fluent_Connections.Models.DataModels.Account;
using Fluent_Connections.Models.DataModels.IoT;
using Fluent_Connections.Models.DataModels.JoinModels;
using Fluent_Connections.Repositories;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fluent_Connections {
    public class AppDbInitialiser {

        public static async Task AddSeededEntries(ApplicationDbContext dbContext,
                UserManager<AppUser> userManager,
                SignInManager<AppUser> signInManager) {

            AppUserRepository appUserRepository = new AppUserRepository(
                dbContext, userManager, signInManager);

            IoTSystemRepository iotSystemRepository = new IoTSystemRepository(
                dbContext);

            StandardUser standardUser = (StandardUser) await
                userManager.FindByNameAsync("Bob@mail.com");

            ClientDevice clientDevice = (ClientDevice)await
                userManager.FindByNameAsync("test");

            /*BeaconDevice beaconDevice =
                await iotSystemRepository.GetBeaconDeviceAsync(2);

            await appUserRepository.DeleteBeaconDeviceReferenceAsync(
                standardUser, clientDevice, beaconDevice);

            await iotSystemRepository.DeleteBeaconDeviceAsync(beaconDevice);*/

            /*BeaconDevice beaconDevice = new BeaconDevice("Hall Way Motion",
                "PIR Sensor", "Detects motion made in the hall way");*/

            /*BeaconDevice beaconDevice = new BeaconDevice("Outdoor Sensor",
                "Temperature Sensor", "Measures the temperature outside");*/

            TemperatureBeacon beaconDevice = new TemperatureBeacon(
                "Default Name", "43234", "4453453", "53453");

            BeaconClientDevice beaconClientDevice =
                new BeaconClientDevice(clientDevice, beaconDevice);

            UserBeaconDevice userBeaconDevice = new UserBeaconDevice(
                "Hallway Activity",
                "Detects whether the hall way is being used", standardUser,
                beaconClientDevice);

            await iotSystemRepository.AddBeaconDeviceAsync(beaconDevice);

            await iotSystemRepository.AddBeaconClientDeviceAsync(
                beaconClientDevice);

            await appUserRepository.AddUserBeaconAsync(
                userBeaconDevice);
        }

    }
}
