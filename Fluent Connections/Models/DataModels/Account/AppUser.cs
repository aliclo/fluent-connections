﻿using Fluent_Connections.Data;
using Fluent_Connections.Models.ViewModels.Account;
using Fluent_Connections.Repositories;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fluent_Connections.Models.DataModels.Account {
    public abstract class AppUser : IdentityUser {

        public long HashId { get; set; }

        public AppUser() {

        }

        public abstract Task<IActionResult> GetHomePage(Controller controller,
            IoTSystemRepository iotSystemRepository);

        public abstract Task<bool> AddDevice(ClientDevice clientDevice,
            AppUserRepository appUserRepository);

        public abstract Task Initialise(ApplicationDbContext dbContext);

    }
}
