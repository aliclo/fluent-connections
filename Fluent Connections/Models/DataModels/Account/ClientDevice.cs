﻿using Fluent_Connections.Data;
using Fluent_Connections.Models.DataModels.JoinModels;
using Fluent_Connections.Models.ViewModels.Account;
using Fluent_Connections.Repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Fluent_Connections.Models.DataModels.Account {
    public class ClientDevice : AppUser {

        public string IpAddress { get; set; }

        [NotMapped]
        public List<StandardUser> StandardUsers { get; set; }

        public ClientDevice() {

        }

        public ClientDevice(DeviceRegisterViewModel deviceRegisterViewModel) {
            this.UserName = deviceRegisterViewModel.Username;
        }

        public override Task<IActionResult> GetHomePage(
                Controller controller,
                IoTSystemRepository iotSystemRepository) {

            return Task.FromResult<IActionResult>(
                controller.View("DeviceHome"));
        }

        public override Task<bool> AddDevice(ClientDevice clientDevice,
                    AppUserRepository appUserRepository) {

            return Task.FromResult(false);
        }

        public override async Task Initialise(ApplicationDbContext dbContext) {
            await LoadStandardUsers(dbContext);
        }

        private async Task LoadStandardUsers(ApplicationDbContext dbContext) {
            IQueryable<StandardUserClientDevice> standardUserClientDevices =
                dbContext.StandardUserClientDevices.Where(
                sucd => sucd.ClientDeviceId.Equals(HashId));

            StandardUsers = await dbContext.StandardUsers.Where(
                su => standardUserClientDevices.Any(
                    sucd => sucd.StandardUserId.Equals(su.HashId)))
                    .ToListAsync();
        }
    }
}
