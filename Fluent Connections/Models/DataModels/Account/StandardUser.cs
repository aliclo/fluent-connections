﻿using Fluent_Connections.Data;
using Fluent_Connections.Models.DataModels.IoT;
using Fluent_Connections.Models.DataModels.JoinModels;
using Fluent_Connections.Models.ViewModels.Account;
using Fluent_Connections.Models.ViewModels.Home;
using Fluent_Connections.Models.ViewModels.IoT;
using Fluent_Connections.Repositories;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Fluent_Connections.Models.DataModels.Account {
    public class StandardUser : AppUser {

        public string Forename { get; set; }
        public string Surname { get; set; }

        [NotMapped]
        public List<ClientDevice> ClientDevices { get; set; }

        public StandardUser() {

        }

        public StandardUser(string forename, string surname) {
            this.Forename = forename;
            this.Surname = surname;
        }

        public StandardUser(StandardRegisterViewModel registerViewModel)
            : base() {

            this.UserName = registerViewModel.EmailAddress;
            this.Email = registerViewModel.EmailAddress;
            this.Forename = registerViewModel.Forename;
            this.Surname = registerViewModel.Surname;
        }

        public override async Task Initialise(ApplicationDbContext dbContext) {
            await LoadClientDevices(dbContext);
        }

        private async Task LoadClientDevices(ApplicationDbContext dbContext) {
            IQueryable<StandardUserClientDevice> standardUserClientDevices =
                dbContext.StandardUserClientDevices.Where(
                sucd => sucd.StandardUserId.Equals(HashId));

            ClientDevices = await dbContext.ClientDevices.Where(
                cd => standardUserClientDevices.Any(
                    sucd => sucd.ClientDeviceId.Equals(cd.HashId)))
                    .ToListAsync();
        }

        public override async Task<IActionResult> GetHomePage(
                Controller controller,
                IoTSystemRepository iotSystemRepository) {

            List<IoTSystem> iotSystems = await
                iotSystemRepository.GetIoTSystemsAsync(this);

            List<UserBeaconDevice> beaconDevices = await
                iotSystemRepository.GetBeaconDevicesAsync(this);

            StandardHomeViewModel standardHomeViewModel =
                new StandardHomeViewModel(iotSystems, beaconDevices);

            return controller.View("StandardHome", standardHomeViewModel);
        }

        public override async Task<bool> AddDevice(ClientDevice clientDevice,
                    AppUserRepository appUserRepository) {

            bool exists = await appUserRepository.AddClientDeviceAsync(
                this, clientDevice);

            return exists;
        }
    }
}
