﻿using Fluent_Connections.Data;
using Fluent_Connections.Models.DataModels.Account;
using Fluent_Connections.Models.DataModels.IoT;
using Fluent_Connections.Models.ViewModels.IoT;
using Fluent_Connections.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fluent_Connections.Models.DataModels.JoinModels {
    public class UserBeaconDevice : IBeaconDevice {

        public string Name { get; set; }
        public string Description { get; set; }

        public long StandardUserHashId { get; set; }
        public StandardUser StandardUser { get; set; }

        public long BeaconDeviceId { get; set; }
        public long ClientDeviceHashId { get; set; }
        public BeaconClientDevice BeaconClientDevice { get; set; }

        public UserBeaconDevice() {

        }

        public UserBeaconDevice(string name, string description,
                StandardUser standardUser,
                BeaconClientDevice beaconClientDevice) {

            this.Name = name;
            this.Description = description;

            this.StandardUserHashId = standardUser.HashId;
            this.StandardUser = standardUser;
            this.BeaconDeviceId = beaconClientDevice.BeaconDeviceId;
            this.ClientDeviceHashId = beaconClientDevice.ClientDeviceHashId;
            this.BeaconClientDevice = beaconClientDevice;
        }

        public void UpdateDetails(
            ModifiedBeaconDeviceViewModel modifiedBeaconDevice) {

            this.Name = modifiedBeaconDevice.Name;
            this.Description = modifiedBeaconDevice.Description;
        }

        public async Task Initialise(ApplicationDbContext dbContext) {
            StandardUser = await dbContext.StandardUsers.SingleAsync(
                u => u.HashId.Equals(StandardUserHashId));

            await StandardUser.Initialise(dbContext);

            BeaconClientDevice =
                await dbContext.BeaconClientDevices.SingleAsync(b =>
                b.BeaconDeviceId.Equals(BeaconDeviceId) &&
                b.ClientDeviceHashId.Equals(ClientDeviceHashId));

            await BeaconClientDevice.Initialise(dbContext);
        }

        public string GetBeaconType() {
            return BeaconClientDevice.BeaconDevice.GetBeaconType();
        }

        public string GetHtmlDisplay() {
            return BeaconClientDevice.BeaconDevice.GetHtmlDisplay();
        }

    }
}
