﻿using Fluent_Connections.Data;
using Fluent_Connections.Models.DataModels.Account;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fluent_Connections.Models.DataModels.JoinModels {
    public class BeaconClientDevice {

        public long BeaconDeviceId { get; set; }
        public BeaconDevice BeaconDevice { get; set; }

        public long ClientDeviceHashId { get; set; }
        public ClientDevice ClientDevice { get; set; }

        public BeaconClientDevice() {

        }

        public BeaconClientDevice(ClientDevice clientDevice,
            BeaconDevice beaconDevice) {

            this.BeaconDeviceId = beaconDevice.Id;
            this.BeaconDevice = beaconDevice;

            this.ClientDeviceHashId = clientDevice.HashId;
            this.ClientDevice = clientDevice;
        }

        public async Task Initialise(ApplicationDbContext dbContext) {
            BeaconDevice = dbContext.BeaconDevices.Single(
                b => b.Id.Equals(BeaconDeviceId));

            ClientDevice = dbContext.ClientDevices.Single(
                c => c.HashId.Equals(ClientDeviceHashId));

            await ClientDevice.Initialise(dbContext);
        }

    }
}
