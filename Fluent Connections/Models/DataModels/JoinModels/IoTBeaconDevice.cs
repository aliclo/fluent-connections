﻿using Fluent_Connections.Models.DataModels.IoT;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Fluent_Connections.Models.DataModels.JoinModels {
    public class IoTBeaconDevice : IBeaconDevice {

        public string Name { get; set; }
        public string Description { get; set; }

        public long StandardUserHashId { get; set; }
        public long BeaconDeviceId { get; set; }
        public long ClientDeviceHashId { get; set; }

        public UserBeaconDevice UserBeaconDevice { get; set; }

        public string IoTSystemId { get; set; }

        public IoTSystem IoTSystem { get; set; }

        public IoTBeaconDevice() {

        }

        public IoTBeaconDevice(IBeaconId beaconId, string iotSystemId) {

            this.StandardUserHashId = beaconId.StandardUserHashId;
            this.BeaconDeviceId = beaconId.BeaconDeviceId;
            this.ClientDeviceHashId = beaconId.ClientDeviceHashId;
            this.IoTSystemId = iotSystemId;
        }

        public IoTBeaconDevice(UserBeaconDevice userBeaconDevice,
                IoTSystem iotSystem) {

            StandardUserHashId = userBeaconDevice.StandardUserHashId;
            BeaconDeviceId = userBeaconDevice.BeaconDeviceId;
            ClientDeviceHashId = userBeaconDevice.ClientDeviceHashId;
            UserBeaconDevice = userBeaconDevice;

            IoTSystemId = iotSystem.Id;
            IoTSystem = iotSystem;

            Name = userBeaconDevice.Name;
            Description = userBeaconDevice.Description;
        }

        public string GetBeaconType() {
            return UserBeaconDevice.BeaconClientDevice.BeaconDevice
                .GetBeaconType();
        }

        public string GetHtmlDisplay() {
            return UserBeaconDevice.BeaconClientDevice.BeaconDevice
                .GetHtmlDisplay();
        }

        /*public IoTBeaconDevice(long standardUserHashId, long beaconDeviceId,
                long clientDeviceHashId, string iotSystemId) {

            StandardUserHashId = standardUserHashId;
            BeaconDeviceId = beaconDeviceId;
            ClientDeviceHashId = clientDeviceHashId;
            IoTSystemId = iotSystemId;
        }*/

    }
}
