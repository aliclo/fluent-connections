﻿using Fluent_Connections.Models.DataModels.Account;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fluent_Connections.Models.DataModels.JoinModels {
    public class StandardUserClientDevice {

        public StandardUserClientDevice() {

        }

        public StandardUserClientDevice(StandardUser standardUser,
                ClientDevice clientDevice) {

            this.StandardUserId = standardUser.HashId;
            this.StandardUser = standardUser;
            this.ClientDeviceId = clientDevice.HashId;
            this.ClientDevice = clientDevice;
        }

        public long StandardUserId { get; set; }
        public StandardUser StandardUser { get; set; }

        public long ClientDeviceId { get; set; }
        public ClientDevice ClientDevice { get; set; }

    }
}
