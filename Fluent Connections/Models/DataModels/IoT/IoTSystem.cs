﻿using Fluent_Connections.Models.DataModels.Account;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Fluent_Connections.Models.DataModels.IoT {
    public class IoTSystem {

        [Key]
        public string Id { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }
        public virtual StandardUser User { get; set; }

        public IoTSystem() {

        }

        public IoTSystem(string name, string description, StandardUser user) {
            this.Name = name;
            this.Description = description;
            this.User = user;
        }

    }
}
