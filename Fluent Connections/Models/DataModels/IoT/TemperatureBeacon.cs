﻿using Fluent_Connections.Models.DataModels.Account;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fluent_Connections.Models.DataModels.IoT {
    public class TemperatureBeacon : BeaconDevice {

        private const string HTML_DISPLAY =
            "<canvas data-type='radial-gauge' " +
            "data-width='200' " +
            "data-height='200' " +
            "data-units='C&#176' " +
            "data-title='Temperature' " +
            "data-min-value='-50' " +
            "data-max-value='50' " +
            "data-major-ticks='[-50,-40,-30,-20,-10,0,10,20,30,40,50]' " +
            "data-minor-ticks='2' " +
            "data-stroke-ticks='true' " +
            "data-highlights='[" +
            "{'from': -50, 'to': 0, 'color': 'rgba(0,0,255,.3)'}, " +
            "{'from': 0, 'to': 50, 'color': 'rgba(255, 0, 0, .3)'}]' " +
            "data-ticks-angle='270' " +
            "data-start-angle='45' " +
            "data-color-major-ticks='#ddd' " +
            "data-color-minor-ticks='#ddd' " +
            "data-color-title='#eee' " +
            "data-color-units='#ccc' " +
            "data-color-numbers='#eee' " +
            "data-color-plate='#222' " +
            "data-border-shadow-width='0' " +
            "data-borders='true' " +
            "data-needle-type='arrow' " +
            "data-needle-width='2' " +
            "data-needle-circle-size='7' " +
            "data-needle-circle-outer='true' " +
            "data-needle-circle-inner='false' " +
            "data-animation-duration='100' " +
            "data-animation-rule='linear' " +
            "data-color-border-outer='#333' " +
            "data-color-border-outer-end='#111' " +
            "data-color-border-middle='#222' " +
            "data-color-border-middle-end='#111' " +
            "data-color-border-inner='#111' " +
            "data-color-border-inner-end='#333' " +
            "data-color-needle-shadow-down='#333' " +
            "data-color-needle-circle-outer='#333' " +
            "data-color-needle-circle-outer-end='#111' " +
            "data-color-needle-circle-inner='#111' " +
            "data-color-needle-circle-inner-end='#222' " +
            "data-color-value-box-rect='#222' " +
            "data-color-value-box-rect-end='#333'></canvas>";

        /*private const string HTML_DISPLAY =
            "<canvas data-type='radial-gauge' " +
            "data-width='300' " +
            "data-height='300'></canvas>";*/

        private const string TEMP_UUID = "A8261B3607EAF5b78846E1363E48B5BE";

        private static readonly Characteristic[] CHARACTERISTICS = {
            new Characteristic(TEMP_UUID)
        };

        private static readonly string CHARACTERISTICS_UUIDS = getUUIDs(
            CHARACTERISTICS);

        public TemperatureBeacon(string name, string modelNumber,
                string serialNumber, string address) : base(name, modelNumber,
                serialNumber, address) {

        }

        public override string GetHtmlDisplay() {
            return HTML_DISPLAY;
        }

        public override Characteristic[] GetCharacteristics() {
            return CHARACTERISTICS;
        }

        public override string GetCharacteristicsUUIDs() {
            return CHARACTERISTICS_UUIDS;
        }

    }
}
