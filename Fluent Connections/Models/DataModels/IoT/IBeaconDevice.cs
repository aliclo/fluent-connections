﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fluent_Connections.Models.DataModels.IoT {
    public interface IBeaconDevice : IBeaconId {

        string Name { get; set; }
        string Description { get; set; }

        string GetBeaconType();
        string GetHtmlDisplay();

    }
}
