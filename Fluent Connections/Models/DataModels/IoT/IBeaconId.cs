﻿using Fluent_Connections.Models.ViewModels.IoT;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fluent_Connections.Models.DataModels.IoT {
    public interface IBeaconId : IBeaconDeviceId {

        long StandardUserHashId { get; set; }
        //long BeaconDeviceId { get; set; }
        long ClientDeviceHashId { get; set; }

    }
}
