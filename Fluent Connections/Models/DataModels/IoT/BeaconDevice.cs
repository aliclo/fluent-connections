﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Fluent_Connections.Data;
using Fluent_Connections.Models.DataModels.IoT;
using Fluent_Connections.Models.ViewModels.IoT;

namespace Fluent_Connections.Models.DataModels.Account {
    public abstract class BeaconDevice {

        private const string TEMP_MN = "43234";

        public static string getUUIDs(Characteristic[] characteristics) {
            string uuids = "";

            for (int ci = 0; ci < characteristics.Length; ci++) {
                uuids += characteristics[ci].UUID;
            }

            return uuids;
        }

        public static BeaconDevice CreateBeaconDevice(
                SelectedBeaconDeviceViewModel beaconDeviceViewModel) {

            string name = beaconDeviceViewModel.Name;
            string modelNumber = beaconDeviceViewModel.ModelNumber;
            string serialNumber = beaconDeviceViewModel.SerialNumber;
            string address = beaconDeviceViewModel.Address;

            switch (beaconDeviceViewModel.ModelNumber) {
                case TEMP_MN: return new TemperatureBeacon(name,
                    modelNumber, serialNumber, address);

                default: return null;
            }
        }

        [Key]
        public long Id { get; set; }

        public string Name { get; set; }
        public string ModelNumber { get; set; }
        public string SerialNumber { get; set; }
        public string Address { get; set; }

        public BeaconDevice(string name, string modelNumber,
                string serialNumber, string address) {

            this.Name = name;
            this.ModelNumber = modelNumber;
            this.SerialNumber = serialNumber;
            this.Address = address;
            //this.Description = description;
            //this.Type = type;
        }

        public BeaconDevice(SelectedBeaconDeviceViewModel
                selectedBeaconDeviceViewModel) {

            this.Name = selectedBeaconDeviceViewModel.Name;
            this.ModelNumber = selectedBeaconDeviceViewModel.ModelNumber;
            this.SerialNumber = selectedBeaconDeviceViewModel.SerialNumber;
            this.Address = selectedBeaconDeviceViewModel.Address;
        }

        public string GetBeaconType() {
            return DeviceTypes.GetTypeName(ModelNumber);
        }

        public abstract string GetHtmlDisplay();

        public abstract Characteristic[] GetCharacteristics();

        public abstract string GetCharacteristicsUUIDs();

    }
}
