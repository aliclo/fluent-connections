﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fluent_Connections.Models.DataModels.IoT {
    public class Characteristic {

        public string UUID { get; }
        public double Value { get; set; }

        public Characteristic(string uuid) {
            UUID = uuid;
        }

    }
}
