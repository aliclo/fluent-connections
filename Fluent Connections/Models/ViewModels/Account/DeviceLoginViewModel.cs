﻿using Fluent_Connections.Repositories;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Fluent_Connections.Models.ViewModels.Account {
    public class DeviceLoginViewModel : LoginViewModel {

        [Required]
        [MinLength(4), MaxLength(30), DataType(DataType.Text)]
        [Display(Name = "Username")]
        public string Username { get; set; }

    }
}
