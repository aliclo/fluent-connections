﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fluent_Connections.Models.ViewModels.Account {
    public class DiscoveredDeviceViewModel {

        public string Name { get; set; }
        public string ModelNumber { get; set; }
        public string SerialNumber { get; set; }
        public string Address { get; set; }
        public string Type { get; set; }
        public string ClientDeviceId { get; set; }

        public DiscoveredDeviceViewModel(string name, string modelNumber,
                string serialNumber, string address, string clientDeviceId) {

            this.Name = name;
            this.ModelNumber = modelNumber;
            this.SerialNumber = serialNumber;
            this.Address = address;
            this.ClientDeviceId = clientDeviceId;
            Type = DeviceTypes.GetTypeName(ModelNumber);
        }

    }
}
