﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fluent_Connections.Models.ViewModels.Account {
    public class DeviceRegisterViewModel : UserRegisterViewModel {

        public string Username { get; set; }
        public string Password { get; set; }

    }
}
