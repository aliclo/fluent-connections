﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Fluent_Connections.Models.ViewModels.Account {
    public class StandardRegisterViewModel : UserRegisterViewModel {

        [Required]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email Address")]
        public string EmailAddress { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Confirm Email Address")]
        [Compare("EmailAddress", ErrorMessage = "Email addresses don't match")]
        public string ConfirmEmailAddress { get; set; }

        [Required]
        [MaxLength(75), DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Required]
        [MaxLength(75), DataType(DataType.Password)]
        [Display(Name = "Confirm Password")]
        [Compare("Password", ErrorMessage = "Passwords don't match")]
        public string ConfirmPassword { get; set; }

        [Required]
        [MinLength(2), MaxLength(30), DataType(DataType.Text)]
        [Display(Name = "Forename")]
        public string Forename { get; set; }

        [Required]
        [MinLength(2), MaxLength(30), DataType(DataType.Text)]
        [Display(Name = "Surname")]
        public string Surname { get; set; }

    }
}
