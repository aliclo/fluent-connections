﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Fluent_Connections.Repositories;
using Microsoft.AspNetCore.Identity;

namespace Fluent_Connections.Models.ViewModels.Account {
    public class StandardLoginViewModel : LoginViewModel {

        [Required]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email Address")]
        public string EmailAddress { get; set; }

    }
}
