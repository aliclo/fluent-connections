﻿using Fluent_Connections.Models.DataModels.Account;
using Fluent_Connections.Models.DataModels.IoT;
using Fluent_Connections.Models.DataModels.JoinModels;
using Fluent_Connections.Models.ViewModels.IoT;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fluent_Connections.Models.ViewModels.Home {
    public class StandardHomeViewModel {

        public List<IoTSystemViewModel> IoTSystems { get; set; } =
            new List<IoTSystemViewModel>();

        public List<BeaconDeviceListItemViewModel> ClientDevices
            { get; set; } = new List<BeaconDeviceListItemViewModel>();

        public StandardHomeViewModel(List<IoTSystem> iotSystems,
                List<UserBeaconDevice> beaconDevices) {

            foreach (IoTSystem iotSystem in iotSystems) {
                IoTSystems.Add(new IoTSystemViewModel(iotSystem));
            }

            foreach (UserBeaconDevice beaconDevice in beaconDevices) {
                ClientDevices.Add(new BeaconDeviceListItemViewModel(
                    beaconDevice));
            }
        }

    }
}
