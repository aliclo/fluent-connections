﻿using Fluent_Connections.Models.DataModels.Account;
using Fluent_Connections.Models.DataModels.IoT;
using Fluent_Connections.Models.DataModels.JoinModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fluent_Connections.Models.ViewModels.IoT {
    public class BeaconDeviceListItemViewModel {

        public long Id { get; set; }

        public string Name { get; set; }
        public string Type { get; set; }
        public string Description { get; set; }

        public BeaconDeviceListItemViewModel(
                UserBeaconDevice userBeaconDevice) {

            this.Id = userBeaconDevice.BeaconDeviceId;
            this.Name = userBeaconDevice.Name;
            this.Type = userBeaconDevice.BeaconClientDevice.BeaconDevice
                .GetBeaconType();

            this.Description = userBeaconDevice.Description;
        }

    }
}
