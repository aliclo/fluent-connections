﻿using Fluent_Connections.Models.DataModels.IoT;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fluent_Connections.Models.ViewModels.IoT {
    public class IoTSystemViewModel {

        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public IoTSystemViewModel(IoTSystem iotSystem) {
            this.Id = iotSystem.Id;
            this.Name = iotSystem.Name;
            this.Description = iotSystem.Description;
        }

    }
}
