﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fluent_Connections.Models.ViewModels.IoT {
    public class SelectedBeaconDeviceViewModel {

        public string Name { get; set; }
        public string ModelNumber { get; set; }
        public string SerialNumber { get; set; }
        public string Address { get; set; }
        public string ClientDeviceId { get; set; }

    }
}
