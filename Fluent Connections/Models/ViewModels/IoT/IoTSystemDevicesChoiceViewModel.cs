﻿using Fluent_Connections.Models.DataModels.JoinModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fluent_Connections.Models.ViewModels.IoT {
    public class IoTSystemDevicesViewModel {

        public string IoTSystemId { get; set; }

        public List<UserBeaconDeviceViewModel> IoTDeviceViewModels
            { get; set; }

        public List<UserBeaconDeviceViewModel> UserBeaconDeviceViewModels
            { get; set; }

        public IoTSystemDevicesViewModel(string iotSystemId,
                List<IoTBeaconDevice> iotBeaconDevices,
                List<UserBeaconDevice> userBeaconDevices) {

            IoTSystemId = iotSystemId;

            UserBeaconDeviceViewModels = new List<UserBeaconDeviceViewModel>();

            foreach(UserBeaconDevice userBeaconDevice in userBeaconDevices) {
                UserBeaconDeviceViewModels.Add(new UserBeaconDeviceViewModel(
                    userBeaconDevice));
            }

            IoTDeviceViewModels = new List<UserBeaconDeviceViewModel>();

            foreach (IoTBeaconDevice iotBeaconDevice in iotBeaconDevices) {
                IoTDeviceViewModels.Add(new UserBeaconDeviceViewModel(
                    iotBeaconDevice));
            }
        }

    }
}
