﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fluent_Connections.Models.ViewModels.IoT {
    public interface IBeaconDeviceId {

        long BeaconDeviceId { get; }

    }
}
