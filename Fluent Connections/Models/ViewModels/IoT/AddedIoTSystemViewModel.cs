﻿using Fluent_Connections.Models.DataModels.IoT;

namespace Fluent_Connections.Models.ViewModels.IoT {
    public class AddedIoTSystemViewModel {

        public string Id { get; set; }

        public AddedIoTSystemViewModel(IoTSystem iotSystem) {
            this.Id = iotSystem.Id;
        }

    }
}
