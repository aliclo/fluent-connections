﻿using Fluent_Connections.Models.DataModels.IoT;
using Fluent_Connections.Models.DataModels.JoinModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fluent_Connections.Models.ViewModels.IoT {
    public class UserBeaconDeviceViewModel {

        public string Name { get; set; }
        public string Type { get; set; }
        public string Description { get; set; }
        public string HtmlDisplay { get; set; }

        public long StandardUserHashId { get; set; }
        public long BeaconDeviceId { get; set; }
        public long ClientDeviceHashId { get; set; }

        public UserBeaconDeviceViewModel(IBeaconDevice beaconDevice) {
            this.Name = beaconDevice.Name;
            this.Type = beaconDevice.GetBeaconType();
            this.Description = beaconDevice.Description;
            this.HtmlDisplay = beaconDevice.GetHtmlDisplay();

            this.StandardUserHashId = beaconDevice.StandardUserHashId;
            this.BeaconDeviceId = beaconDevice.BeaconDeviceId;
            this.ClientDeviceHashId = beaconDevice.ClientDeviceHashId;
        }

    }
}
