﻿using Fluent_Connections.Models.DataModels.IoT;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fluent_Connections.Models.ViewModels.IoT {
    public class UserBeaconDeviceIDViewModel : IBeaconId {

        public long StandardUserHashId { get; set; }
        public long BeaconDeviceId { get; set; }
        public long ClientDeviceHashId { get; set; }

    }
}
