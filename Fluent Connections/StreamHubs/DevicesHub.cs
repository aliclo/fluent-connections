﻿using Fluent_Connections.Data;
using Fluent_Connections.Models.DataModels.Account;
using Fluent_Connections.Models.DataModels.JoinModels;
using Fluent_Connections.Models.ViewModels.Account;
using Fluent_Connections.Repositories;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Channels;
using System.Threading.Tasks;

namespace Fluent_Connections.StreamHubs {
    public class DevicesHub : Hub {

        private static int COUNT = 0;

        private int myCount;
        private bool _streamFinished = false;

        private readonly Dictionary<string, int> _querierConnections =
            new Dictionary<string, int>();

        private readonly ApplicationDbContext _dbContext;
        private readonly AppUserRepository _appUserRepository;
        private readonly IoTSystemRepository _ioTSystemRepository;

        public DevicesHub(ApplicationDbContext dbContext,
                UserManager<AppUser> userManager,
                SignInManager<AppUser> signInManager) {

            _dbContext = dbContext;

            _appUserRepository = new AppUserRepository(dbContext, userManager,
                signInManager);

            _ioTSystemRepository = new IoTSystemRepository(dbContext);
        }

        public override async Task OnDisconnectedAsync(Exception exception) {
            //_streamFinished = true;
            await base.OnDisconnectedAsync(exception);
        }

        public ChannelReader<float> RequestSensorData(long beaconDeviceId,
                CancellationToken cancellationToken) {

            Channel<float> channel = Channel.CreateUnbounded<float>();

            WriteSensorData(beaconDeviceId, channel.Writer, cancellationToken);

            return channel.Reader;
        }

        public ChannelReader<DiscoveredDeviceViewModel> ScanForDevices(
                CancellationToken cancellationToken) {

            Channel<DiscoveredDeviceViewModel> channel =
                Channel.CreateUnbounded<DiscoveredDeviceViewModel>();

            WriteDiscoveredDevices(channel.Writer, cancellationToken);

            return channel.Reader;
        }

        private async Task WriteSensorData(long beaconDeviceId,
                ChannelWriter<float> writer,
                CancellationToken cancellationToken) {

            myCount = COUNT;
            COUNT++;

            StandardUser user = (StandardUser) await
                _appUserRepository.GetUserAsync(Context.User);

            long userId = user.HashId;

            UserBeaconDevice userBeacon = await _ioTSystemRepository.
                GetBeaconDeviceAsync(user, beaconDeviceId);

            string clientIpAddress = userBeacon.BeaconClientDevice
                .ClientDevice.IpAddress;

            string beaconBtAddress = userBeacon.BeaconClientDevice
                .BeaconDevice.Address;

            BeaconClientDevice beaconClient = userBeacon.BeaconClientDevice;
            BeaconDevice beacon = beaconClient.BeaconDevice;
            string characteristicsUUIDs = beacon.GetCharacteristicsUUIDs();

            WebClient client = new WebClient();
            client.QueryString.Add("Address", beaconBtAddress);
            client.QueryString.Add("CharaUUIDS", characteristicsUUIDs);
            Stream stream = await client.OpenReadTaskAsync(
                "http://" + clientIpAddress + ":80/SensorData");

            StreamReader streamReader = new StreamReader(stream);

            try {
                while(!_streamFinished) {
                    cancellationToken.ThrowIfCancellationRequested();
                    string numStr = await streamReader.ReadLineAsync();
                    //int num = int.Parse(numStr);
                    float num = ConvertToFloat(numStr);
                    await writer.WriteAsync(num);
                }
            } catch (Exception e) {
                writer.TryComplete(e);
            }

            writer.TryComplete();

            stream.Close();
        }

        private async Task WriteDiscoveredDevices(
                ChannelWriter<DiscoveredDeviceViewModel> writer,
                CancellationToken cancellationToken) {

            StandardUser user = (StandardUser)await
                _appUserRepository.GetUserAsync(Context.User);

            await user.Initialise(_dbContext);

            ClientDevice device = user.ClientDevices.ElementAt(0);

            /*_client.BaseAddress = new Uri("http://" + device.IpAddress + ":80/");
            _client.DefaultRequestHeaders.Accept.Clear();
            _client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));

            HttpResponseMessage response = await _client.GetAsync("test");

            if(response.IsSuccessStatusCode) {
                List<string> test = await response.Content.ReadAsAsync<List<string>>();
                int num = test.Count;
            }*/

            WebClient client = new WebClient();
            Stream stream = await client.OpenReadTaskAsync(
                "http://" + device.IpAddress + ":80/ScanBeacons");

            StreamReader streamReader = new StreamReader(stream);

            try {
                await WriteReceivedDevices(user, device, writer,
                    streamReader);
            } catch (Exception e) {
                writer.TryComplete(e);
            }

            writer.TryComplete();

            stream.Close();
        }

        private async Task WriteReceivedDevices(AppUser user,
                ClientDevice device,
                ChannelWriter<DiscoveredDeviceViewModel> writer,
                StreamReader streamReader) {

            string deviceName = await streamReader.ReadLineAsync();
            string deviceModelNumber = await streamReader.ReadLineAsync();
            string deviceSerialNumber = await streamReader.ReadLineAsync();
            string address = await streamReader.ReadLineAsync();

            while (deviceName != null) {
                DiscoveredDeviceViewModel discoveredDeviceViewModel =
                    new DiscoveredDeviceViewModel(deviceName,
                    deviceModelNumber, deviceSerialNumber, address, device.Id);

                await WriteDevice(user, discoveredDeviceViewModel, writer);

                deviceName = await streamReader.ReadLineAsync();
                deviceModelNumber = await streamReader.ReadLineAsync();
                deviceSerialNumber = await streamReader.ReadLineAsync();
                address = await streamReader.ReadLineAsync();
            }
        }

        private async Task WriteDevice(AppUser user,
                DiscoveredDeviceViewModel discoveredDeviceViewModel,
                ChannelWriter<DiscoveredDeviceViewModel> writer) {

            string address = discoveredDeviceViewModel.Address;

            bool alreadyHasBeaconDevice =
                await _ioTSystemRepository.HasBeaconDeviceAsync(user, address);

            if (!alreadyHasBeaconDevice) {
                await writer.WriteAsync(discoveredDeviceViewModel);
            }
        }

        private float ConvertToFloat(string bytesFormattedStr) {
            string[] bytesStr = bytesFormattedStr.Split('|');
            byte[] bytes = new byte[bytesStr.Length];
            for(int i = 0; i < bytes.Length; i++) {
                bytes[i] = byte.Parse(bytesStr[i]);
            }

            int sign = bytes[3] >> 7;
            int exponent = (bytes[3] << 1) + ((bytes[2] & (1 << 7)) >> 7);
            float exponentResult = (float) Math.Pow(2, exponent-127);

            float result = 0;

            int div = 2;

            for (int i = 6; i >= 0; i--) {
                result += exponentResult * ((bytes[2] & (1 << i)) >> i) / div;

                div <<= 1;
            }

            for (int i = 1; i >= 0; i--) {
                for(int j = 7; j >= 0; j--) {
                    result += exponentResult * ((bytes[i] & (1 << j)) >> j)/div;
                    div <<= 1;
                }
            }

            result += exponentResult;

            return -sign*result + (1-sign) * result;
        }

        /*public ChannelReader<DiscoveredDeviceViewModel> ScanForDevices(
                CancellationToken cancellationToken) {

            Channel<DiscoveredDeviceViewModel> channel = Channel.CreateUnbounded<DiscoveredDeviceViewModel>();

            DiscoveredDeviceViewModel[] list = {
                new DiscoveredDeviceViewModel("Hall Way Sensor", "PIR Sensor"),
                new DiscoveredDeviceViewModel("Outdoor Sensor", "Temperature Sensor"),
                new DiscoveredDeviceViewModel("Hall Way Light", "LED")
            };

            SendStringsAsync(list, channel.Writer, cancellationToken);

            return channel.Reader;
        }*/

            /*public async Task SendStringsAsync(DiscoveredDeviceViewModel[] list,
                    ChannelWriter<DiscoveredDeviceViewModel> writer,
                    CancellationToken cancellationToken) {

                try {
                    for (int i = 0; i < list.Length; i++) {
                        cancellationToken.ThrowIfCancellationRequested();
                        await writer.WriteAsync(list[i]);
                        await Task.Delay(1000, cancellationToken);
                    }
                } catch (Exception e) {
                    writer.TryComplete(e);
                }

                writer.TryComplete();
            }*/

    }
}
